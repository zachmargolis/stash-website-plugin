# Stash Website Plugin

This plugin will allow files from Stash to be served up directly
allowing Stash to host simple websites.

Currently there is no link to the file, you have to access it directly:

    http://host:port/www/$PROJECT/$REPO/$BRANCH/$PATH
