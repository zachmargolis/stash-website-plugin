package com.atlassian.stash.plugin.website;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.content.ContentService;
import com.atlassian.stash.content.ContentTreeNode;
import com.atlassian.stash.exception.NoSuchPathException;
import com.atlassian.stash.io.TypeAwareOutputSupplier;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.util.CachePolicies;
import com.atlassian.stash.rest.util.ResourcePatterns;
import com.sun.jersey.spi.resource.Singleton;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;

@Singleton
@AnonymousAllowed
@Path(ResourcePatterns.REPOSITORY_URI)
public class WebsiteResource {

    private final ContentService contentService;

    public WebsiteResource(ContentService contentService) {
        this.contentService = contentService;
    }

    @GET
    @Path("{at}/{path:.*}")
    public Response getFile(@Context final HttpServletResponse response, @Context final Repository repository,
                            @PathParam("at") final String at, @PathParam("path") String path) throws IOException {
        try {
            ContentTreeNode.Type nodeType = contentService.getType(repository, at, path);
            final String fullPath;
            if (nodeType == ContentTreeNode.Type.DIRECTORY) {
                if (!path.endsWith("/") && !path.isEmpty()) {
                    // If file doesn't end with '/' the browser requests the wrong resources
                    // Evil but seems to work
                    response.sendRedirect(new File(path).getName() + "/");
                    return null;
                }
                fullPath = path + "index.html";
            } else {
                fullPath = path;
            }

            return Response.ok(new StreamingOutput() {
                @Override
                public void write(final OutputStream output) throws IOException, WebApplicationException {
                    contentService.streamFile(repository, at, fullPath, new TypeAwareOutputSupplier() {
                        @Override
                        public OutputStream getStream(@Nonnull String contentType) throws IOException {

                            return output;
                        }
                    });
                }
            }).type(URLConnection.guessContentTypeFromName(fullPath))
                    .cacheControl(CachePolicies.getCacheControlObjectId(at))
                    .build();
        } catch (NoSuchPathException e) {
            // Don't log
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
    }
}
